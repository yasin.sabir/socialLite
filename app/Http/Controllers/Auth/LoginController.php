<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider_github()
    {
        return Socialite::driver('github')->redirect();
    }

    public function redirectToProvider_google()
    {
        return Socialite::driver('google')->redirect();
    }



    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback_github()
    {
        $github_user = Socialite::driver('github')->user();
        $user = User::where(['provider_id'=> $github_user->getId()])->first();
        if(!$user){
            $user = User::create([
                'name' => $github_user->getNickname(),
                'email' => $github_user->getEmail(),
                'provider_id' => $github_user->getId(),
                'provider' => 'github'
            ]);
        }
        Auth::login($user,true);
        return redirect($this->redirectTo);

        // $user->token;
    }



    public function handleProviderCallback_google()
    {
        $google_user = Socialite::driver('google')->user();
        //dd($google_user);

        $user = User::where(['provider_id'=> $google_user->getId()])->first();

        if(!$user){
            $user = User::create([
                'name' => $google_user->getName(),
                'email' => $google_user->getEmail(),
                'provider_id' => $google_user->getId(),
                'provider' => 'google'
            ]);
        }
        Auth::login($user,true);
        return redirect($this->redirectTo);

        // $user->token;
    }


}
